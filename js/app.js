let DB;

// selectores de la interfaz
const form = document.querySelector('form'),
    nombreMascota = document.querySelector('#mascota'),
    nombreCliente = document.querySelector('#cliente'),
    telefono = document.querySelector('#telefono'),
    fecha = document.querySelector('#fecha'),
    hora = document.querySelector('#hora'),
    sintomas = document.querySelector('#sintomas'),
    citas = document.querySelector('#citas'),
    headingAdministra = document.querySelector('#administra');

// esperar por el DOM ready
document.addEventListener('DOMContentLoaded', () => {
    // crear la Base de Datos
    let crearDB = window.indexedDB.open('citas', 1);

    // si hay un error enviarlo a la consola
    crearDB.onerror = () => {
        console.log('Hubo un error');
    }

    // si todo esta bien mostrar en consola y asignar la Base de Datos
    crearDB.onsuccess = () => {
        console.log('Todo Listo!!');

        // asignar a la Base de datos
        DB = crearDB.result;

        mostrarCitas();
    }

    // este metodo solo corre una vez y es ideal para crear el shema
    crearDB.onupgradeneeded = (e) => {
        // el evento es la misam base de datos
        let db = e.target.result;

        // definir el objectStore, toma 2 parametros el nombre de la BD y segundo las opciones
        // keyPath es el indice de la BD
        let objectStore = db.createObjectStore('citas', { keyPath: 'key', autoIncrement: true });

        // crear los indices y campos de la base de datos, createIndex: 3, nombre, keypath y opciones
        objectStore.createIndex('mascota', 'mascota', { unique: false });
        objectStore.createIndex('cliente', 'cliente', { unique: false });
        objectStore.createIndex('telefono', 'telefono', { unique: false });
        objectStore.createIndex('fecha', 'fecha', { unique: false });
        objectStore.createIndex('hora', 'hora', { unique: false });
        objectStore.createIndex('sintomas', 'sintomas', { unique: false });


    }

    // cuando el formulario se envia
    form.addEventListener('submit', agregarDatos);

    function agregarDatos (e) 
    {
        e.preventDefault();
        
        const nuevaCita = {
            mascota : nombreMascota.value,
            cliente : nombreCliente.value,
            telefono : telefono.value,
            fecha : fecha.value,
            hora : hora.value,
            sintomas : sintomas.value
        }

        // en indexedDB se utilizan las transacciones
        let transaction = DB.transaction(['citas'], 'readwrite');
        let objectStore = transaction.objectStore('citas');
        let request = objectStore.add(nuevaCita);

        request.onsuccess = () => {
            form.reset();
        }

        transaction.oncomplete = () => {
            console.log('cita agregada');
            mostrarCitas();
        }

        transaction.onerror = () => {
            console.log('Hubo un error...');
        }
    }

    function mostrarCitas () 
    {
        // limpiar las citas anteriores
        while(citas.firstChild) {
            citas.removeChild(citas.firstChild);
        }

        // creamos un objectStore
        let objectStore = DB.transaction('citas').objectStore('citas');

        // esto retorna una petidicion
        objectStore.openCursor().onsuccess = (e) => {
            // cursor se va a hubibar en el registro indicado para acceder a la BD
            let cursor = e.target.result;

            if (cursor) {
                let citasHTML = document.createElement('li');
                citasHTML.setAttribute('data-cita-id', cursor.value.key);
                citasHTML.classList.add('list-group-item');
                citasHTML.innerHTML = `
                    <p class="font-weight-bold">Mascota: <span class="font-weight-normal">${cursor.value.mascota}</span></p>
                    <p class="font-weight-bold">Cliente: <span class="font-weight-normal">${cursor.value.cliente}</span></p>
                    <p class="font-weight-bold">Telefono: <span class="font-weight-normal">${cursor.value.telefono}</span></p>
                    <p class="font-weight-bold">Fecha: <span class="font-weight-normal">${cursor.value.fecha}</span></p>
                    <p class="font-weight-bold">Hora: <span class="font-weight-normal">${cursor.value.hora}</span></p>
                    <p class="font-weight-bold">Sintomas: <span class="font-weight-normal">${cursor.value.sintomas}</span></p>
                `;

                // borrar bottom
                const botonBorrar = document.createElement('button');
                botonBorrar.classList.add('borrar', 'btn', 'btn-danger');
                botonBorrar.innerHTML = '<span aria-hidden="true">x</span> Borrar';
                botonBorrar.onclick = borrarCitas;
                citasHTML.appendChild(botonBorrar);

                // append en el padre
                citas.appendChild(citasHTML);

                // consultar los proximos registros
                cursor.continue();
            } else {
                titleListCita();
            }
        }
    }

    function borrarCitas (e)
    {
        let citaId = Number(e.target.parentElement.getAttribute('data-cita-id'));

        // en indexedDB se utilizan las transacciones
        let transaction = DB.transaction(['citas'], 'readwrite');
        let objectStore = transaction.objectStore('citas');
        let request = objectStore.delete(citaId);

        transaction.oncomplete = () => {
            e.target.parentElement.parentElement.removeChild(e.target.parentElement);

            titleListCita();
        }
    }

    function titleListCita () {
        if (!citas.firstChild) {
            // cuando no hay registros
            headingAdministra.textContent = 'Agrega Citas para comenzar';
            let listado = document.createElement('p');
            listado.classList.add('text-center');
            listado.textContent = 'No hay registros';
            citas.appendChild(listado);
        } else {
            headingAdministra.textContent = 'Admistra tus Citas';
        }
    }
});
